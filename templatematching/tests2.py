import matplotlib.pyplot as plt
import numpy as np
import templateMatching
import time
import cv2
import transfer_cut_ver2 as trs
from imageSimilarityMeasures import ImageSimilarityMeasure


def objective_function(args):
    x = round(args['x1'])
    y = round(args['x2'])
    alpha = args['aplha']
    scale = args['scale']

    cut_image = trs.transform_cutGRAY(image, template.shape, x, y, alpha, scale)
    cut_image = np.uint8(cut_image)
    similarity_measure = ism.bhattacharyya_measure(cut_image)

    return similarity_measure


# Test program
show_progress_on_image = False
show_final_results = False

num_of_runs = 1
num_of_images = 309

num_of_templates_searched = 0
num_of_good_template_center_searches = 0
num_of_good_scale_searches = 0
num_of_good_angle_searches = 0

for i in range(0, 20):
    if i is 184 or i is 202:
        continue

    print("\n!!!!!!Image {0}!!!!!!".format(i))
    image_path = 'tennis_ball_dataset/{0}.jpg'.format(i)
    template_path = 'tennis_ball_dataset/{0}_ref.jpg'.format(i)
    image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
    template = cv2.cvtColor(cv2.imread(template_path), cv2.COLOR_BGR2RGB)

    params = {'x1': (50, image.shape[0] - 50),
              'x2': (50, image.shape[1] - 50),
              'scale': (0.7, 3.5),
              'aplha': (-45, 45)}

    ism = ImageSimilarityMeasure(template)
    ism.initialize(32)

    for run in range(0, num_of_runs):
        # print("\n!!!!!!RUN {0}!!!!!!".format(run))
        num_bees = 50
        num_scouts = 3
        num_of_iter = 10*num_scouts

        # print("\nTesting with num_bees: {}".format(num_bees))
        ABC = templateMatching.Colony(objective_function, params, num_bees=num_bees, max_iter=1, limit=5, num_scouts=num_scouts)

        if show_progress_on_image:
            template_fig = plt.figure()
            plt.imshow(template, cmap='gray')
            template_fig.show()
            fig = plt.figure()
            fig.show()

        iteration_no_gain = 0
        ABC.fit()
        best_result = ABC.best_food
        for j in np.arange(0, num_of_iter, 1):
            ABC.fit()

            if abs(best_result - ABC.best_food) <= 0.02:
                iteration_no_gain += 1
            else:
                iteration_no_gain = 0
            best_result = ABC.best_food
            if iteration_no_gain > 5:
                break

            # print('Best:', ABC.best_position, ABC.best_food)

            if show_progress_on_image:
                fig.clear()
                plt.imshow(image, cmap='gray')
                for bee in ABC.colony:
                    plt.scatter(bee.position['x2'], bee.position['x1'])
                plt.plot(ABC.best_position['x2'], ABC.best_position['x1'], 'rx',  markersize=15, linewidth=9)
                fig.show()

                time.sleep(0.001)
                plt.pause(0.001)

            # plt.waitforbuttonpress(100)

        # print('Final Best:', ABC.best_position, ABC.best_food)

        template_data_file_path = 'tennis_ball_dataset/{0}_1.txt'.format(i)
        template_data_file = open(template_data_file_path, 'r')

        values = [value for value in template_data_file][0].split(' ')
        template_data = {'x2_center': float(values[0]), 'x1_center': float(values[1]),
                'x2_width': float(values[2]), 'x1_width': float(values[3])}
        # print('Template data: {}'.format(template_data))
        coordinates_tolerance = [0.25 * template_data['x1_width'], 0.25 * template_data['x2_width']]
        # print('Coordinates tolerance: {}'.format(coordinates_tolerance))

        num_of_templates_searched += 1
        if abs(ABC.best_position['x1'] - template_data['x1_center']) <= coordinates_tolerance[0]\
                and abs(ABC.best_position['x2'] - template_data['x2_center']) <= coordinates_tolerance[1]:
            # print('Good search')
            num_of_good_template_center_searches += 1
            if -20 <= ABC.best_position['aplha'] <= 20:
                # print('Good angle')
                num_of_good_angle_searches += 1
            if 0.9 <= ABC.best_position['scale'] <= 1.2:
                num_of_good_scale_searches += 1
                # print('Good scale')


        if show_final_results:
            # plt.close()
            # plt.close()
            fig1 = plt.figure()
            plt.imshow(image)
            # plt.hold(True)

            r = np.sqrt((template.shape[0]/2.0)**2 + (template.shape[1]/2.0)**2) * ABC.best_position['scale']
            a = np.arctan2(template.shape[0], template.shape[1])
            a1 = np.arctan2(template.shape[1], template.shape[0])
            a2 = a + (-ABC.best_position['aplha']/180) * np.pi
            a21 = -a1 + (-ABC.best_position['aplha']/180) * np.pi

            x1 = [ABC.best_position['x1'] + r * np.sin(a2),
                  ABC.best_position['x1'] + r * np.sin(a21),
                  ABC.best_position['x1'] + r * np.sin(a2 + np.pi),
                  ABC.best_position['x1'] + r * np.sin(a21 + np.pi),
                  ABC.best_position['x1'] + r * np.sin(a2)]

            x2 = [ABC.best_position['x2'] + r * np.cos(a2),
                  ABC.best_position['x2'] + r * np.cos(a21),
                  ABC.best_position['x2'] + r * np.cos(a2 + np.pi),
                  ABC.best_position['x2'] + r * np.cos(a21 + np.pi),
                  ABC.best_position['x2'] + r * np.cos(a2)]

            plt.plot(x2, x1, linewidth=3)
            fig1.show()

            # fig = plt.figure()
            # fig.show()
            # plt.imshow(template)

            plt.pause(10)
            plt.close()


print('Number of templates searched: {}'.format(num_of_templates_searched))
print('Number of good template center searches: {}'.format(num_of_good_template_center_searches))
print('Number of good scale searches: {}'.format(num_of_good_scale_searches))
print('Number of good angle searches: {}'.format(num_of_good_angle_searches))

