import matplotlib.pyplot as plt
import numpy as np
import templateMatching
import time
import cv2
import transfer_cut_ver2 as trs
from imageSimilarityMeasures import ImageSimilarityMeasure

test_images = [
        ['../dataset/air_baloon.JPG', '../dataset/air_baloon_template.JPG'],
        ['../dataset/air_baloon_2.JPG', '../dataset/air_baloon_2_template.JPG'],
        ['../dataset/air_baloon_3.JPG', '../dataset/air_baloon_3_template.JPG'],
        ['../dataset/air_baloon_4.JPG', '../dataset/air_baloon_4_template.JPG'],
        ['../dataset/air_baloon_5.JPG', '../dataset/air_baloon_5_template.JPG'],
        ['../dataset/EarthObservation/image.jpg', '../dataset/EarthObservation/template.jpg']
        ]
test_select = 4

# tenissBall dataset
# tenis_im = 14
# im_path = '\%i.jpg' % tenis_im
# template_path = '\%i_ref.jpg' % tenis_im
# ref_path = '\%i.txt' % tenis_im

# Read images
# image = cv2.cvtColor(cv2.imread(im_path), cv2.COLOR_BGR2RGB)
# template = cv2.cvtColor(cv2.imread(template_path), cv2.COLOR_BGR2RGB)
# image = cv2.cvtColor(cv2.imread(test_images[test_select][0]), cv2.COLOR_BGR2RGB)
# template = cv2.cvtColor(cv2.imread(test_images[test_select][1]), cv2.COLOR_BGR2RGB)
image_path = 'tennis_ball_dataset/{0}.jpg'.format(282)
template_path = 'tennis_ball_dataset/{0}_ref.jpg'.format(282)
image = cv2.cvtColor(cv2.imread(image_path), cv2.COLOR_BGR2RGB)
template = cv2.cvtColor(cv2.imread(template_path), cv2.COLOR_BGR2RGB)

ism = ImageSimilarityMeasure(template)
ism.initialize(32)


def objective_function(args):
    x = round(args['x1'])
    y = round(args['x2'])
    alpha = args['aplha']
    scale = args['scale']

    cut_image = trs.transform_cutGRAY(image, template.shape, x, y, alpha, scale)
    cut_image = np.uint8(cut_image)
    similarity_measure = ism.bhattacharyya_measure(cut_image)

    return similarity_measure


params = {'x1': (50, image.shape[0]-50),
          'x2': (50, image.shape[1]-50),
          'scale': (0.7, 3.5),
          'aplha': (-45, 45)}

# Test program
num_bees_possibilities = []
num_bees_possibilities_results = {}
for num_bees in [3, 5, 9, 14, 22, 35, 50, 70, 100]:
    num_bees_possibilities.append(num_bees)
    num_bees_possibilities_results[num_bees] = []

num_scouts_possibilities = []
num_scouts_possibilities_results = {}
for num_scouts in []:
    num_scouts_possibilities.append(num_scouts)
    num_scouts_possibilities_results[num_scouts] = []

show_progress_on_image = False
show_final_results = False

print(num_bees_possibilities)
num_of_runs = 20
for run in range(0, num_of_runs):
    print("\n!!!!!!RUN {0}!!!!!!".format(run))
    for num_bees in num_bees_possibilities_results:
        num_scouts = 5
        num_of_iter = 10*num_scouts

        # print("\nTesting with num_bees: {}".format(num_bees))
        ABC = templateMatching.Colony(objective_function, params, num_bees=num_bees, max_iter=1, limit=5, num_scouts=num_scouts)

        if show_progress_on_image:
            template_fig = plt.figure()
            plt.imshow(template, cmap='gray')
            template_fig.show()
            fig = plt.figure()
            fig.show()

        iteration_no_gain = 0
        ABC.fit()
        best_result = ABC.best_food
        for i in np.arange(0, num_of_iter, 1):
            ABC.fit()

            if abs(best_result - ABC.best_food) <= 0.02:
                iteration_no_gain += 1
            else:
                iteration_no_gain = 0
            best_result = ABC.best_food
            if iteration_no_gain > 5:
                break

            # print('Best:', ABC.best_position, ABC.best_food)

            if show_progress_on_image:
                fig.clear()
                plt.imshow(image, cmap='gray')
                for bee in ABC.colony:
                    plt.scatter(bee.position['x2'], bee.position['x1'])
                plt.plot(ABC.best_position['x2'], ABC.best_position['x1'], 'rx',  markersize=15, linewidth=9)
                fig.show()

                time.sleep(0.001)
                plt.pause(0.001)

            # plt.waitforbuttonpress(100)

        num_bees_possibilities_results[num_bees].append(ABC.best_food)
        # print('Final Best:', ABC.best_position, ABC.best_food)

        if show_final_results:
            # plt.close()
            # plt.close()
            fig1 = plt.figure()
            plt.imshow(image)
            # plt.hold(True)

            r = np.sqrt((template.shape[0]/2.0)**2 + (template.shape[1]/2.0)**2) * ABC.best_position['scale']
            a = np.arctan2(template.shape[0], template.shape[1])
            a1 = np.arctan2(template.shape[1], template.shape[0])
            a2 = a + (-ABC.best_position['aplha']/180) * np.pi
            a21 = -a1 + (-ABC.best_position['aplha']/180) * np.pi

            x1 = [ABC.best_position['x1'] + r * np.sin(a2),
                  ABC.best_position['x1'] + r * np.sin(a21),
                  ABC.best_position['x1'] + r * np.sin(a2 + np.pi),
                  ABC.best_position['x1'] + r * np.sin(a21 + np.pi),
                  ABC.best_position['x1'] + r * np.sin(a2)]

            x2 = [ABC.best_position['x2'] + r * np.cos(a2),
                  ABC.best_position['x2'] + r * np.cos(a21),
                  ABC.best_position['x2'] + r * np.cos(a2 + np.pi),
                  ABC.best_position['x2'] + r * np.cos(a21 + np.pi),
                  ABC.best_position['x2'] + r * np.cos(a2)]

            plt.plot(x2, x1, linewidth=3)
            fig1.show()

            # fig = plt.figure()
            # fig.show()
            # plt.imshow(template)

            plt.pause(3)
            plt.close()

for num_bees in num_bees_possibilities:
    avg_best = sum(num_bees_possibilities_results[num_bees])/len(num_bees_possibilities_results[num_bees])
    print('Average best for num_bees {0} : {1}'.format(num_bees, avg_best))

