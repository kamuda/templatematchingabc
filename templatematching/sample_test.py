import matplotlib.pyplot as plt
import numpy as np
import templateMatching
import time
import cv2 
import transfer_cut_ver2 as trs
from imageSimilarityMeasures import ImageSimilarityMeasure

test_images = [
        ['../dataset/air_baloon.JPG', '../dataset/air_baloon_template.JPG'],
        ['../dataset/air_baloon_2.JPG', '../dataset/air_baloon_2_template.JPG'],
        ['../dataset/air_baloon_3.JPG', '../dataset/air_baloon_3_template.JPG'],
        ['../dataset/air_baloon_4.JPG', '../dataset/air_baloon_4_template.JPG'],
        ['../dataset/air_baloon_5.JPG', '../dataset/air_baloon_5_template.JPG'],
        ['../dataset/EarthObservation/image.jpg', '../dataset/EarthObservation/template.jpg']
        ]
test_select = 250;

# tenissBall dataset
tenis_im = 14
im_path = '/media/skm/7BB2-0A2E/tennis_ball_datasets/%i.jpg'%tenis_im
template_path = '/media/skm/7BB2-0A2E/tennis_ball_datasets/%i_ref.jpg'%(tenis_im)
ref_path = '/media/skm/7BB2-0A2E/tennis_ball_datasets/%i.txt'%tenis_im



# Read images
image = cv2.cvtColor(cv2.imread(im_path), cv2.COLOR_BGR2RGB)
template =  cv2.cvtColor(cv2.imread(template_path), cv2.COLOR_BGR2RGB)
plt.close()
plt.close()
plt.close()
plt.close()

def compare_images(image, ism):
#    assert image.shape == pattern.shape;
#    ret = ism.bhattacharyya_measure(image)
    image = np.uint8(image)
    ret = ism.x2measure(image)
    return ret



def check_similatiry_distribution():
    ism = ImageSimilarityMeasure(template)
    ism.initialize(32)
    cost = np.zeros(image.shape)
    for i in range(0, image.shape[0]):
        if np.mod(i, 10)==0:
            print(i)
            
        for j in range(0, image.shape[1]):
            im = trs.transform_cutGRAY(image, template.shape, i, j, 0, 1)
            cost[i, j] = compare_images(im, ism)
    fig = plt.figure() 
    [x, y]=np.where(cost==cost.min());       
    plt.imshow(cost)
    plt.colorbar()
    plt.scatter(y, x)
    fig.show()
    return cost
    

ism = ImageSimilarityMeasure(template)
ism.initialize(32)
      

def test_fun_2(args):
    x = round(args['x1'])
    y = round(args['x2'])
    alpha = args['aplha']
    scale = args['scale']
    
    im = trs.transform_cutGRAY(image, template.shape, x, y, alpha, scale)
    im = np.uint8(im)
    ret = ism.bhattacharyya_measure(im)
    return ret


params = {'x1': (50, image.shape[0]-50), 
          'x2': (50, image.shape[1]-50),
          'scale': (0.7, 3.5),
          'aplha': (-45, 45)
         }

# Test program
ABC  = []
ABC = templateMatching.Colony(test_fun_2, params, num_bees=20, max_iter=10, limit=50)
template_fig = plt.figure()
plt.imshow(template, cmap='gray')
template_fig.show()

fig = plt.figure()
fig.show()
best_pos = None
iteration_no_gain = 0

for i in np.arange(1, 250, 1):
    ABC.fit()
    print('Best:', ABC.best_position, ABC.best_food)
    
    fig.clear()
    plt.imshow(image, cmap='gray')
    for bee in ABC.colony:
        plt.scatter(bee.position['x2'], bee.position['x1'])
    plt.plot(ABC.best_position['x2'], ABC.best_position['x1'], 'rx',  markersize=15, linewidth=9)
    #plt.axis((params['x1'][0], params['x1'][1], params['x2'][0], params['x2'][1]))
    fig.show()
   
    if(best_pos != ABC.best_position):
        best_pos = ABC.best_position;
        iteration_no_gain = 0;
    else:
        iteration_no_gain = iteration_no_gain+1;
    if iteration_no_gain > 8:
        break
    time.sleep(0.001)
    plt.pause(0.001)
    
    #plt.waitforbuttonpress(100)

    print('Best:', ABC.best_position, ABC.best_food)
   
    
plt.close()    
plt.close()    
fig = plt.figure()
fig.show()
plt.imshow(image)
plt.hold(True)

r = np.sqrt((template.shape[0]/2.0)**2 + (template.shape[1]/2.0)**2) * best_pos['scale']
a = np.arctan2(template.shape[0], template.shape[1]);
a1 = np.arctan2(template.shape[1], template.shape[0]);
a2 = a + (ABC.best_position['aplha']/180) * np.pi
a21 = -a1 + (ABC.best_position['aplha']/180) * np.pi

x1 = [best_pos['x1']+r*np.sin(a2), 
      best_pos['x1']+r*np.sin(a21), 
      best_pos['x1']+r*np.sin(a2+np.pi), 
      best_pos['x1']+r*np.sin(a21+np.pi), 
      best_pos['x1']+r*np.sin(a2)]

x2 = [best_pos['x2']+r*np.cos(a2), 
      best_pos['x2']+r*np.cos(a21), 
      best_pos['x2']+r*np.cos(a2+np.pi), 
      best_pos['x2']+r*np.cos(a21+np.pi), 
      best_pos['x2']+r*np.cos(a2)]

plt.plot(x2, x1, linewidth=3)

fig = plt.figure()
fig.show()
plt.imshow(template)