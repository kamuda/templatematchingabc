clear all;
close all;

for i = 0:183
    fileID = fopen(['tennis_ball_dataset\', num2str(i) , '.txt'], 'r');
    coordinates = fscanf(fileID, '%f');

    image = imread(['tennis_ball_dataset\', num2str(i) , '.jpg']);
    [height, width, channels] = size(image);

    template_width = round(coordinates(4)*width);
    template_height = round(coordinates(5)*height);
    template_width_center = coordinates(2)*width;
    template_height_center = coordinates(3)*height;

    x1 = ceil(template_width_center - template_width/2);
    x2 = floor(template_width_center + template_width/2);
    y1 = ceil(template_height_center - template_height/2);
    y2 = floor(template_height_center + template_height/2);

%     fileID = fopen(['tennis_ball_dataset\', num2str(i) , '_1.txt'], 'w');
%     coordinates = fprintf(fileID, '%f %f %f %f', template_width_center, template_height_center, template_width, template_height);

    template = image(y1:y2, x1:x2, :);
    template4x = imresize(template, 4);
    template05x = imresize(template, 0.5);

    figure; imshow(template);
    figure; imshow(template4x);
    figure; imshow(template05x);

    imwrite(template4x, ['tennis_ball_dataset\', num2str(i) , '_refx4.jpg'])
    imwrite(template05x, ['tennis_ball_dataset\', num2str(i) , '_refx05.jpg'])
end

