clear all;
close all;

num_of_bees = [3, 5, 9, 14, 22, 35, 50, 70, 100];

% Number of iterations = 30
% x2 = [0.57, 0.52, 0.41, 0.41, 0.33, 0.25, 0.17, 0.17, 0.18];

% Number of iterations = 50
x2 = [0.48, 0.45, 0.36, 0.35, 0.28, 0.19, 0.22, 0.20, 0.16];


figure; plot(num_of_bees, x2, 'm*', 'MarkerSize', 15);
title({'Algorithm effectiveness for different working bees amounts', 'with maximum number of iterations = 50'});
xlabel('Number of working bees');
ylabel('x2 measure');
