import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt

#Funkcje przesuwające, obracajce i skalujące: 1) obrazy w skali szarości, 2) obrazy RGB
#Argumenty: image_in             - obraz przeszukiwany w skali szarości / RGB
#           pat_ysize, pat_xsize - rozmiary obrazu wzorca
#           dx                   - przesunięcie w osi poziomej
#           dy                   - przesunięcie w osi pionowej
#           angle                - kąt obrotu wobec punktu środkowego wzorca
#           scale                - współczynnik skalujący obraz wzorca
#Output:    image_out            - obraz o wielkości obrazu przeszukiwanego, zawierający przesunięty, obrócony i przeskalowany obraz wzorca


def get_image_section(Im, siz, x_center, y_center):
    x_start = int(x_center - np.floor(siz[0] / 2))
    y_start = int(y_center - np.floor(siz[1] / 2))
    x_end = x_start + siz[0]
    y_end = y_start + siz[1]
    
    x2_start = 0
    y2_start = 0
    x2_end = siz[0]
    y2_end = siz[1]
    
    if x_start<0:
        x2_start = 0 - x_start
        x_start = 0
    if y_start<0:
        y2_start = 0 - y_start
        y_start = 0
    if x_end > Im.shape[0]:
        x2_end -= x_end - Im.shape[0] 
        x_end = Im.shape[0]  
    if y_end > Im.shape[1]:
        y2_end -= y_end - Im.shape[1] 
        y_end = Im.shape[1]
    #print('args: ', x_center, y_center)    
    #print('in:  ' , x2_start , x2_end, y2_start , y2_end )
    #print('out: ' , x_start , x_end, y_start , y_end )
    
    ret = np.zeros(siz);
    ret[x2_start : x2_end, y2_start : y2_end] = Im[x_start : x_end, y_start : y_end] 
    
    return ret



def transform_cutGRAY(image_in, pattern_size, dx, dy, angle, scale):   #pat_ysize - wysokosc, pat_xsize - szerokosc
# Works also for RGB    
   
    if scale>=1:
        py = int(pattern_size[0]*scale*1.42)
        px = int(pattern_size[1]*scale*1.42)
    else:
        py = int(pattern_size[0]*1.42)
        px = int(pattern_size[1]*1.42)
    #Zdefiniowanie wielkosci wycinka
    if len(pattern_size)>2:
        siz = (py, px, 3)
    else:     
        siz = (py, px)
        
    wycinek = get_image_section(image_in, siz, dx, dy)
    '''
    
    if(scale > 1):
        cut_size = np.floor(np.sqrt(2)*0.5*scale*pat_xsize+np.sqrt(2)*0.5*scale*pat_ysize)
    else:
        cut_size = np.floor(np.sqrt(2)*0.5*pat_xsize+np.sqrt(2)*0.5*pat_ysize)

    print(cut_size)

    #Rozszerzenie obrazu tak aby wyciąć można było fragmenty z brzegów obrazu wejściowego
    half_cut_size = np.int(np.floor(cut_size/2))

    new_image_in = np.zeros((image_in.shape[0]+np.int(cut_size), image_in.shape[1]+np.int(cut_size)))
    new_image_in[half_cut_size:half_cut_size+image_in.shape[0], half_cut_size:half_cut_size+image_in.shape[1]] = image_in

    #Wycięcie fragmentu przeszukiwanego obrazu zgodnie z dx, dy
    wycinek = new_image_in[np.int(half_cut_size+dy-np.floor(cut_size/2)):np.int(half_cut_size+dy+np.floor(cut_size/2))+1, np.int(half_cut_size+dx-np.floor(cut_size/2)):np.int(half_cut_size+dx+np.floor(cut_size/2))+1]
    '''
    #Obracanie i skalowanie wycinka
    rotate_matrix = cv.getRotationMatrix2D((
            np.floor(wycinek.shape[0]/2), np.floor(wycinek.shape[1]/2)), 
            -angle, 1/scale)
    transf_wycinek = cv.warpAffine(wycinek, rotate_matrix, (wycinek.shape[1], wycinek.shape[0]))
    y_id = int(wycinek.shape[0] / 2 - pattern_size[0] / 2)
    x_id = int(wycinek.shape[1] / 2 - pattern_size[1] / 2)
    image_out = transf_wycinek[y_id : y_id+pattern_size[0],
                               x_id : x_id+pattern_size[1]]

    return image_out


def transform_cutRGB(image_in, pattern_size, dx, dy, angle, scale):
    pat_ysize = pattern_size[0]
    pat_xsize = pattern_size[1]
    # Zdefiniowanie wielkosci wycinka
    if (scale > 1):
        cut_size = np.floor(np.sqrt(2) * 0.5 * scale * pat_xsize + np.sqrt(2) * 0.5 * scale * pat_ysize)
    else:
        cut_size = np.floor(np.sqrt(2) * 0.5 * pat_xsize + np.sqrt(2) * 0.5 * pat_ysize)

   

    # Rozszerzenie obrazu tak aby wyciąć można było fragmenty z brzegów obrazu wejściowego
    half_cut_size = np.int(np.floor(cut_size / 2))

    new_image_in = np.zeros((image_in.shape[0] + np.int(cut_size), image_in.shape[1] + np.int(cut_size), 3))
    new_image_in[half_cut_size:half_cut_size + image_in.shape[0],
    half_cut_size:half_cut_size + image_in.shape[1], :] = image_in

    # Wycięcie fragmentu przeszukiwanego obrazu zgodnie z dx, dy
    wycinek = new_image_in[np.int(half_cut_size + dy - np.floor(cut_size / 2)):np.int(
        half_cut_size + dy + np.floor(cut_size / 2)) + 1, np.int(half_cut_size + dx - np.floor(cut_size / 2)):np.int(
        half_cut_size + dx + np.floor(cut_size / 2)) + 1, :]

    # Obracanie i skalowanie wycinka
    rotate_matrix = cv.getRotationMatrix2D((np.floor(cut_size / 2), np.floor(cut_size / 2)), -angle, 1 / scale)
    transf_wycinek = cv.warpAffine(wycinek, rotate_matrix, (wycinek.shape[1], wycinek.shape[0]))
    image_out = transf_wycinek[
                half_cut_size - np.int(np.floor(pat_ysize / 2)):half_cut_size + np.int(np.floor(pat_ysize / 2)) + 1,
                half_cut_size - np.int(np.floor(pat_xsize / 2)):half_cut_size + np.int(np.floor(pat_xsize / 2)) + 1, :]

    return image_out

if __name__ == "__main__": 
    # Wczytanie obrazu przeszukiwanego oraz obrazu wzorca
    image = cv.imread('../dataset/air_baloon.JPG')
    pattern = cv.imread('../dataset/air_baloon_template.JPG')
    
    image_gr = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
    pattern_gr = cv.cvtColor(pattern, cv.COLOR_BGR2GRAY)
    
    #Wczytanie obroconego, przeskalowanego fragmentu
    test_wzor = image #cv.imread('test.bmp')
    test_wzor_gr = image_gr #cv.cvtColor(test_wzor, cv.COLOR_BGR2GRAY)
    
    #pattern_gr.shape[0] - wysokość wzorca
    #pattern_gr.shape[1] - szerokość wzorca
    wycinekTEST = transform_cutGRAY(test_wzor_gr, pattern_gr.shape, 37, 27, 60, 0.3)
    wycinekTEST_RGB = np.uint8(transform_cutRGB(test_wzor, pattern.shape, 37, 27, 60, 0.3))
    
    plt.figure(1)
    plt.subplot(2, 2, 1)
    plt.imshow(image_gr)
    plt.title('Przeszukiwany obraz')
    
    plt.subplot(2, 2, 2)
    plt.imshow(cv.cvtColor(test_wzor, cv.COLOR_BGR2RGB))
    plt.title('Wycinek do obrocenia')
    
    plt.subplot(2, 2, 3)
    plt.gray()
    plt.imshow(wycinekTEST)
    plt.title('Wycinek TEST')
    
    plt.subplot(2, 2, 4)
    plt.gray()
    plt.imshow(cv.cvtColor(wycinekTEST_RGB, cv.COLOR_BGR2RGB))
    plt.title('Wycinek TEST RGB')
    plt.show()
    
    
    
    x = 452
    y = 552
    test = test_wzor_gr
    test[x-1:x+1,y-1:y+1] = 0
    wycinekTEST = transform_cutGRAY(test, pattern_gr.shape, x, y, 0, 1)
    plt.imshow(wycinekTEST)