import numpy as np
import cv2 as cv
import math
from pyscipopt import Model, quicksum, multidict
from sklearn.cluster import KMeans
from sklearn.preprocessing import StandardScaler


# TO DO: DOCSTRINGS
class ImageSimilarityMeasure:
    def __init__(self, reference_image, num_of_clusters=5):
        self.reference_image = reference_image
        self.num_of_clusters = num_of_clusters
        self.hist_size = None
        self.ref_image_prob_dist = None
        self.ref_image_histogram = None
        self.ref_image_signature = None

    def initialize(self, hist_size):
        """ This method must be called before using the object. """
        self.hist_size = hist_size
        self.ref_image_prob_dist, self.ref_image_histogram = self._calculate_image_probability_distribution(self.reference_image)
        self.ref_image_signature = self._compute_image_signature(self._convertRGB2CIELabvector(self.reference_image))

    def bhattacharyya_measure(self, compared_image):
        compared_image_prob_dist, _ = self._calculate_image_probability_distribution(compared_image)

        bhattacharyya_coefficient = np.sum(np.sqrt(self.ref_image_prob_dist*compared_image_prob_dist))
        BD = -math.log(bhattacharyya_coefficient+0.00001)

        return BD

    def x2measure(self, compared_image):
        _, compared_image_histogram = self._calculate_image_probability_distribution(compared_image)

        x2 = np.sum(np.nan_to_num(((compared_image_histogram - self.ref_image_histogram) ** 2) /
                                  (compared_image_histogram + self.ref_image_histogram)))

        return x2

    def emd_based_measure(self, compared_image):
        compared_CIELabvector = self._convertRGB2CIELabvector(compared_image)

        signature1 = self.ref_image_signature
        signature2 = self._compute_image_signature(compared_CIELabvector)
        wp = [number for (_, number) in signature1.values()]
        wq = [number for (_, number) in signature2.values()]
        m = len(wp)
        n = len(wq)
        d = self._compute_emd_ground_distance_matrix(signature1, signature2)

        optimal_value, sum_f = self._earth_movers_distance(m, n, d, wp, wq)
        measure = optimal_value/sum_f

        return measure

    def _calculate_image_probability_distribution(self, image):
        image_histogram = cv.calcHist([image], [0, 1, 2], None, [self.hist_size, self.hist_size, self.hist_size], [0, 256, 0, 256, 0, 256])
        image_prob_dist = image_histogram/np.sum(image_histogram)

        return image_prob_dist, image_histogram

    def _convertRGB2CIELabvector(self, image):
        image_shape = image.shape
        imageCIELab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
        CIELabvector = imageCIELab.reshape((-1, image_shape[-1]))

        return CIELabvector

    def _compute_image_signature(self, imageCIELabvector):
        imageCIELabvector = StandardScaler().fit_transform(imageCIELabvector)
        estimator = KMeans(n_clusters=self.num_of_clusters).fit(imageCIELabvector)
        image_clusters = estimator.fit(imageCIELabvector).cluster_centers_
        image_signature = {i: (image_clusters[i, :], np.where(estimator.labels_ == i)[0].shape[0]) for i in range(estimator.n_clusters)}

        return image_signature

    def _compute_emd_ground_distance_matrix(self, signature1, signature2):
        features1 = [feature for (feature, _) in signature1.values()]
        features2 = [feature for (feature, _) in signature2.values()]
        emd_ground_distance_matrix = np.empty((len(features1), len(features2)))
        for i in range(len(features1)):
            for j in range(len(features2)):
                emd_ground_distance_matrix[i, j] = np.linalg.norm(features1[i] - features2[j])

        return emd_ground_distance_matrix

    def _earth_movers_distance(self, m, n, d, wp, wq):
        model = Model("emd")

        # Create variables
        f = {}
        for i in range(m):
            for j in range(n):
                f[i, j] = model.addVar(vtype="C", name="f(%s,%s)" % (i, j))

        # P constraints
        for i in range(m):
            model.addCons(quicksum(f[i, j] for j in range(n)) <= wp[i])

        # Q constraints
        for j in range(1, n):
            model.addCons(quicksum(f[i, j] for i in range(m)) <= wq[j])

        sum_wp = sum(wp[i] for i in range(m))
        sum_wq = sum(wq[j] for j in range(n))
        sum_f = min(sum_wp, sum_wq)
        model.addCons(quicksum(f[i, j] for (i, j) in f) == sum_f)

        # Objective
        model.setObjective(quicksum(d[i, j] * f[i, j] for (i, j) in f), "minimize")

        model.hideOutput()
        model.optimize()
        optimal_value = model.getObjVal()

        return optimal_value, sum_f


if __name__ == "__main__":
    reference_image = cv.imread('database/baloons/air_baloon_template.JPG', cv.IMREAD_GRAYSCALE)
    compared_image = cv.imread('database/baloons/air_baloon_template_modified.jpg', cv.IMREAD_GRAYSCALE)
    reference_imageRGB = cv.imread('database/baloons/air_baloon_template.JPG', cv.COLOR_BGR2RGB)
    compared_imageRGB = cv.imread('database/baloons/air_baloon_template_modified.jpg', cv.COLOR_BGR2RGB)
    compared_imageRGB2 = cv.imread('wzor1.jpg', cv.COLOR_BGR2RGB)

#      cv.imshow('air_baloon_template', reference_image)
#      cv.imshow('air_baloon_template_modified', compared_image)
#      cv.waitKey()

    ism = ImageSimilarityMeasure(reference_imageRGB)
    ism.initialize(32)

    BD = ism.bhattacharyya_measure(compared_imageRGB)
    x2 = ism.x2measure(compared_imageRGB)
    emd_based_measure = ism.emd_based_measure(compared_imageRGB)

    print("Bhattacharyya measure value: ", BD)
    print("X2 measure value: ", x2)
    print("EMD based measure value: ", emd_based_measure)

    # image_histogram = cv.calcHist([image], [0, 1, 2], None, [8, 8, 8], [0, 256, 0, 256, 0, 256])

    print('end')

